# -*- coding: utf-8 -*-
"""
Created on Sat Aug 22 22:20:21 2020
@author: pramod kumar
@description: python special data types
"""

"""
Number Data Type in Python:
    There are three different data types available for number in python. they are defined as int,float,complex
    
Python List Methods
append() - Add an element to the end of the list
extend() - Add all elements of a list to the another list
insert() - Insert an item at the defined index
remove() - Removes an item from the list
pop() - Removes and returns an element at the given index
clear() - Removes all items from the list
index() - Returns the index of the first matched item
count() - Returns the count of the number of items passed as an argument
sort() - Sort items in a list in ascending order
reverse() - Reverse the order of items in the list
copy() - Returns a shallow copy of the list    
"""
import os
print(os.getcwd())

a = 10
print(type(a))
print(type(4.2))
c = 22+3j
print(c)
print(isinstance(c,complex))

#type casting
print(int(4.5))
print(float(-2))
print(complex(3+4j))

print(0.4)

#python fractions
import fractions
print(fractions.Fraction(1.5))
print(fractions.Fraction(4,6))
print(fractions.Fraction('1.1'))

"""
Python offers modules like math and random to carry out different mathematics like trigonometry, logarithms, probability and statistics, etc.
"""
#import math
#import random

"""
How to create a List
List is created by [] by placing all items inside it and sepreated by commas.its mutable and can have any number of items and they may be different data types
"""
my_list = []
mylist = [1,2,3,4,5]
mylist_a = [1,4,5,"hello",5.6]
mylist_n = ['Hello',[1,2,3,4,5]]
print("last element",mylist[-1])
print(mylist_a[3])
print(mylist_n[0][4])
print(mylist_n[1][3])

my_list = ['p','r','o','g','r','a','m','i','z']
# elements 3rd to 5th
print(my_list[2:5])
# elements beginning to 4th
print(my_list[3])
# elements 6th to end
print(my_list[5:])
#begineeing to end
print(my_list[:])

print(12/5)
print(11%2)

#How to change or add elements to a list?
#List are mutable, meaning their elements can be changed

# Correcting mistake values in a list
odd = [2, 4, 6, 8]
# change the 1st item 
odd[0] = 4
# change 2nd to 4th items
odd[1:] = 9,8,7
print(odd)

#changing/add/removing elements from list
#we can add single item using append or multiple items at once using extend method

odd = [2, 4, 6, 8]
odd.append(7)
print(odd)
odd.extend([4,6,9])
print(odd)
del(odd[1])
print(odd)

#demo of list for insert method
odd = [1,9]
odd.insert(1,3)
print(odd)
odd[2:2] = [8,8]
print(odd)

#modifications of list 
my_list = ['p','r','o','b','l','e','m']
my_list.remove('p')
print(my_list)
# Output: ['r', 'o', 'b', 'l', 'e', 'm']

print(my_list.pop(1))
# Output: 'o'

print(my_list)
# Output: ['r', 'b', 'l', 'e', 'm']

print(my_list.pop())
#output m


print(my_list)
# Output: ['r', 'b', 'l', 'e']

my_list.clear()
print(my_list)
# Output: []

# Python list methods
my_list = [3, 8, 1, 6, 0, 8, 4]

# Output: 1
print(my_list.index(8))

# Output: 2
print(my_list.count(8))

my_list.sort()

# Output: [0, 1, 3, 4, 6, 8, 8]
print(my_list)

my_list.reverse()

# Output: [8, 8, 6, 4, 3, 1, 0]
print(my_list)

#creating a list with each item increasing power of 2
for y in range(10):
    print(y)
    
pow2 = [2**x for x in range(10)]
print(pow2)

for fruit in ['apple','banana','mango']:
    print('I like fruit',fruit)



