# -*- coding: utf-8 -*-
"""
Created on Tue Aug 25 22:51:12 2020
@author: pramod kumar
@description:tuples data types
"""

"""
Tuples is similar to list. its created by () by putting any data types elements within it.
tuples is immutable it can not change directly.parenthessis is optional but its good practice to use ()
"""
#different types of tuples
#creating a tuple
mytuple = ()
print(mytuple)

#integer elements
mytuple = (1,2,3,4,5,2,2,2)
print(mytuple.count(2))

#mixed
mytuple = (1,"hello",3.4)
print(mytuple.index('hello'))

#mixed
mytuple = (3,4,[3,5,9],(43,5,99))
del mytuple
#print(mytuple[1])
#print(mytuple[1][0])
test = 3,4,5
print(test)
a,b,c=test
print(a)

#single element
single_t = ('hello',)
print(single_t)

changet = list(test)
print(changet)
changet[0]=10
print(changet)
#print(mytuple[1][2])

"""Advantanges of tuples
tuples are faster due to immutable.
tuples can be use as key for dictionary
tupes are used generally for hetrogenious data and list used for homogenious data.
"""

