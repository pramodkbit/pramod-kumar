# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 18:28:07 2020
@author: pramod kumar
@description:sets
"""
"""
Sets: sets is immutable ie means item can not change and only add or remove from it. its a colleciton of unordered elements with unique elements.
"""
my_set = {1,2,3}
print(my_set)

my_set = {5.5,"hello",(1,2,3)}
print(my_set)

#my_set = {1,2,3,[4,5,6]}
#print(my_set)

d = {}
print(type(d))

a = set()
print(type(a))

"""
Modifying a set in Python:
Sets are mutable. However, since they are unordered, indexing has no meaning. 
We cannot access or change an element of a set using indexing or slicing.
Set data type does not support it.
We can add a single element using the add() method,
and multiple elements using the update() method. 
The update() method can take tuples, lists, strings or other sets as its argument. 
In all cases, duplicates are avoided.   
"""
a = {1,2,3}
a.add(5)
print(a)
a.update([6,9])
print(a)
a.update([3,4],{7,8,9})
print(a)
#remove an element
a.discard(3)
print(a)
#this will give an error if element is not found
a.remove(1)
print(a)
a.clear()
print(a)

str = set('pramod')
print(str)

"""Python Set Operations:
set supports union,intersection,difference,symmetric difference    
"""
a = {1,2,3,4,5,6}
b = {4,5,6,7,8,9}
#union method
print(a | b) 
print(a.union(b))

#intersection ie common elements in both sets
print(a & b)
print(a.intersection(b))

#Set Difference
#Difference of the set B from set A(A - B) is a set of elements that are only in A but not in B
#Similarly, B - A is a set of elements in B but not in A.
print(a-b)
print(a.difference(b))

#Set Symmetric Difference
#get elements from both set excluding common elements
print(a^b)
print(a.symmetric_difference(b))

for letter in set('apple'):
    print(letter)