# -*- coding: utf-8 -*-
"""
Created on Wed Aug 26 22:31:29 2020
@author: @pramod kumar
@description: strings
"""
"""
Strings: strings is a sequence of unicode characters
"""
my_string = 'hello'
print(my_string)
my_string = "Hello"
print(my_string)
my_string = '''Hello'''
print(my_string)
my_string = """Hello python
love you very much and please come in my life very strongly"""
print(my_string)

#my_string[1] = "k"
#print(my_string)

del my_string
#print(my_string)

# Python String Operations
str1 = "hello"
str2 = " pramod"
print('str1+str2=',str1+str2)
print('str1*3=',str1*3)

# Iterating through a string
count = 0
for letter in 'Hello World':
    if(letter == 'l'):
        count += 1
print(count,'letters found')

print('Hello'.lower())
print('hello'.upper())
