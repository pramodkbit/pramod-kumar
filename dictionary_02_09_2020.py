# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 23:36:14 2020
@author: pramod kumar
@description: dictionary
"""
#Creating a dictionary is as simple as placing items inside curly braces {} separated by commas.

# empty dictionary
my_dict = {}
print(my_dict)
# dictionary with integer keys
my_dict = {1: 'apple', 2: 'ball'}
print(my_dict)
# dictionary with mixed keys
my_dict = {'name': 'John', 1: [2, 4, 3]}
print(my_dict)
# using dict()
my_dict = dict({1:'apple', 2:'ball'})
print(my_dict)
# from sequence having each item as a pair
my_dict = dict([(1,'apple'), (2,'ball')])
print(my_dict)

#dict can be create using curly brackes or dict keyword