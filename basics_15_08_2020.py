# -*- coding: utf-8 -*-
"""
Created on Sat Aug 15 23:35:07 2020

@author: pc
"""


"""
Spyder Editor

This is a temporary script file.
"""
#print("Hello Pramod")
"""List is an ordered sequence of items. values of its items can be changed.It is one of the most used datatype in Python and is very flexible. All the items in a list do not need to be of the same type."""
fruits = ["apple","manago","banana"] #list
number = (1,2,3,4,5)#tuple
alphabets = {'a':'apple','b':'banana','c':"coconut"}#dictionary
alpha_set = {'a','b','c','d'} #set
print(fruits)
print(number)
print(alphabets)
print(alpha_set)
b= 1+2j
type(b)
list_v = [5,10,15,20,25,30,35,40]
print("a[2]=",list_v[2])
print("a[0:3]=",list_v[1:3])
print("a[5:]=",list_v[5:])
a = [1,2,3]
a[2]= 4
print(a)
"""Tuple is an ordered sequence of items same as a list. The only difference is that tuples are immutable. Tuples once created cannot be modified."""
"""String is sequence of Unicode characters. We can use single quotes or double quotes to represent strings. Multi-line strings can be denoted using triple quotes, """
name = "pramod"
hello = 'kumar'
print(name)
print(hello)
'''Set is an unordered collection of unique items. Set is defined by values separated by comma inside braces { }. Items in a set are not ordered.'''
set_v = {1,2,3,4}
print(set_v)

print(int(1.5))
print(float(10))
convert_set  = set([1,2,3]) # converting list into set
print(convert_set)

convert_tuple  = tuple({1,2,3}) # converting set into tuple
print(convert_tuple)

convert_str  = list('hello') # converting string to list
print(convert_str)

#convert 2d into dictionary
convert_dct = dict([[1,2],[3,4]])
print(convert_dct)

convert_dct1 = dict([(1,2),(3,4)])
print(convert_dct1)

x = 5;y=10
print('the value of x is {} and y is {}'.format(x,y))
print('I like to love eat {0} and drink {1}'.format('butter','milk'))

x = 12.3456789
print('the value of x is %3.2f' %x)
print('the value of x is %3.4f' %x)

'''number_a = input('Input a number')

import math
from math import pi
print(pi)
'''
def outer_function():
    a=20
    def inner_function():
        a = 30
        print('a=',a)
    inner_function()
    print('a=',a)
    
a = 10
outer_function()
print('a=',a)
    


