# -*- coding: utf-8 -*-
"""
Created on Wed Aug 19 09:35:31 2020
@author: pramod kumar
@description: python flow control and python functions
"""

num = 10
if(num>10):
    print("Number is greater than 10")
else:
    print("number is less than 10")
    
print("always print statement works")

new_num = -3.4 #float(input("Enter a number"))
if(new_num>0):
    print(new_num,' is greate than 0')
elif(new_num==0):
    print('new number is 0')
else:
    print('given number is negative')

#program to find sum of all numbers given in list.
list_num = [11,33,10,20,30]
sum = 0
for i in list_num:
    sum = sum+i
print(sum)    

#get factorial of given number
def fact(num):
    if(num==1):
        return 1
    else:
        return(num * fact(num-1))
fact_num = 6        
print('factorial of given number ',fact_num," is ",fact(fact_num))    

#lambda function: ananonymous function is a function that is defined without a name. 
#anonymous functions are defined using the lambda keyword.
double_lam = lambda x:x*2  
print(double_lam(10))  
#program to filter item only even number from list 
mylist = list(range(1,20))
print(mylist)
mylist_lamda = list(filter(lambda x:(x%2==0),mylist))
print(mylist_lamda)

#Program to double each item in a list using map()
mylist_double = list(map(lambda x:x*3,mylist_lamda))
print(mylist_double)

##global variables and local variables
xx = 'global'
def foo():
    global xx
    y = 'local'
    xx = xx*2
    print(xx)
    print(y)
foo()   

"""
Nonlocal Variables
non local variables are used in nested functions where local scope is not defined.
"""
def outer():
    x = "local variables"
    def inner():
        nonlocal x
        x = 'inner function'
        print(x)
        
    inner()
    print("inner function",x)    
    
    outer()